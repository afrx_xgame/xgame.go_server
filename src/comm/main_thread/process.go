package main_thread

import (
	"sync/atomic"
	"xgame.go_server/comm/log"
)

// 主队列大小
const mainQSize = 2048

// 主队列
var mainQ = make(chan func(), mainQSize)

// 是否已经开始工作
var started int32 = 0

//
// Process 主线程处理过程,
// 将 task 插入主队列并不立即执行!
func Process(task func()) {
	if nil == task {
		return
	}

	if len(mainQ) >= mainQSize {
		log.Warning("主队列过于拥挤, task 被抛弃")
		return
	}

	mainQ <- task

	if atomic.CompareAndSwapInt32(&started, 0, 1) {
		// 开始循环执行任务
		startLoopExecTask()
	}
}

// 循环执行任务
func startLoopExecTask() {
	go loopExecTask()
}

// 循环执行任务
func loopExecTask() {
	for {
		task := <-mainQ

		if nil == task {
			continue
		}

		func() {
			defer func() { // 类似于 try
				if err := recover(); nil != err { // 类似于 catch
					log.Error("发生异常, %+v", err)
				}
			}()

			// 执行任务
			task()
		}()
	}
}
