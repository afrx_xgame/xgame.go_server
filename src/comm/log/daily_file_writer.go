package log

import (
	"io"
	"os"
	"path"
	"sync"
	"time"

	"github.com/pkg/errors"
)

//
// 日志文件写手
type dailyFileWriter struct {
	// 文件名称
	fileName string
	// 上一次写入日期
	lastYearDay int
	// 输出文件
	outputFile *os.File
	// 文件交换锁
	fileSwitchLock sync.Mutex
}

// @override
func (this *dailyFileWriter) Write(byteArray []byte) (n int, err error) {
	if nil == byteArray ||
		len(byteArray) <= 0 {
		return 0, nil
	}

	// 获取输出文件
	outputFile, err := this.getOutputFile()

	if nil != err {
		return 0, err
	}

	// 将日志打印到屏幕和写入文件
	_, _ = os.Stdout.Write(byteArray)
	_, _ = outputFile.Write(byteArray)

	return len(byteArray), nil
}

// 获取输出文件,
// 每天创建一个新的日志文件
func (this *dailyFileWriter) getOutputFile() (io.Writer, error) {
	yearDay := time.Now().YearDay()

	if this.lastYearDay == yearDay &&
		nil != this.outputFile {
		return this.outputFile, nil
	}

	this.fileSwitchLock.Lock()
	defer this.fileSwitchLock.Unlock()

	if this.lastYearDay == yearDay &&
		nil != this.outputFile {
		return this.outputFile, nil
	}

	this.lastYearDay = yearDay

	// 构建日志目录
	err := os.MkdirAll(path.Dir(this.fileName), os.ModePerm)

	if nil != err {
		return nil, errors.Wrap(err, "创建目录失败")
	}

	// 定义新的日志文件
	newDailyFile := this.fileName + "." + time.Now().Format("20060102")

	outputFile, err := os.OpenFile(
		newDailyFile,
		os.O_CREATE|os.O_APPEND|os.O_WRONLY,
		0644,
	)

	if nil != err ||
		nil == outputFile {
		return nil, errors.Errorf("打开文件 %s 失败! err = %s", newDailyFile, err)
	}

	if nil != this.outputFile {
		// 关闭原来的文件
		_ = this.outputFile.Close()
	}

	this.outputFile = outputFile
	return outputFile, nil
}
