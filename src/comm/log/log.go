package log

import (
	"fmt"
	"log"
	"runtime"
)

var writer *dailyFileWriter
var debugLogger, infoLogger, warningLogger, errorLogger *log.Logger

//
// Config 配置日志
func Config(outputFileName string) {
	writer = &dailyFileWriter{
		fileName:    outputFileName,
		lastYearDay: -1,
	}

	debugLogger = log.New(
		writer, "[ DEBUG ] ",
		log.Ltime|log.Lmicroseconds|log.Lshortfile,
	)

	infoLogger = log.New(
		writer, "[ INFO ] ",
		log.Ltime|log.Lmicroseconds|log.Lshortfile,
	)

	warningLogger = log.New(
		writer, "[ WARNING ] ",
		log.Ltime|log.Lmicroseconds|log.Lshortfile,
	)

	errorLogger = log.New(
		writer, "[ ERROR ] ",
		log.Ltime|log.Lmicroseconds|log.Lshortfile,
	)
}

// Debug 记录调试日志
func Debug(format string, valArray ...interface{}) {
	_ = debugLogger.Output(
		2,
		fmt.Sprintf(format, valArray...),
	)
}

// Info 记录消息日志
func Info(format string, valArray ...interface{}) {
	_ = infoLogger.Output(
		2,
		fmt.Sprintf(format, valArray...),
	)
}

// Warning 记录警告日志
func Warning(format string, valArray ...interface{}) {
	_ = warningLogger.Output(
		2,
		fmt.Sprintf(format, valArray...),
	)
}

// Error 记录错误日志
func Error(format string, valArray ...interface{}) {
	_ = errorLogger.Output(
		2,
		fmt.Sprintf(format, valArray...),
	)

	for i := 5; ; i++ {
		_, file, line, ok := runtime.Caller(i)

		if !ok {
			break
		}

		_ = errorLogger.Output(
			2,
			fmt.Sprintf("    at %+v:%+v", file, line),
		)
	}
}
