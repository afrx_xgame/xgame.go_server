package lazy_save

import "time"

// 心跳时间
const heartbeatTime = time.Second

// 稳定时间
const steadyTime = 20 * time.Second
