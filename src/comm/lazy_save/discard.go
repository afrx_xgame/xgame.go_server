package lazy_save

import "xgame.go_server/comm/log"

//
// Discard 放弃延迟保存
func Discard(oldObj LazySaveObj) {
	if nil == oldObj {
		return
	}

	log.Warning(
		"放弃延迟保存, lsoId = %s",
		oldObj.GetLsoId(),
	)

	innerMap.Delete(oldObj.GetLsoId())
}
