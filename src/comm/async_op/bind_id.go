package async_op

import "hash/crc32"

//
// BindId 绑定 Id
type BindId int64

//
// 将绑定 Id 转型为 int
func (that BindId) ToInt() int {
	return int(that)
}

//
// 将绑定 Id 转型为 int32
func (that BindId) ToInt32() int32 {
	return int32(that)
}

//
// StrToBindId 将字符串转成 BindId
func StrToBindId(val string) BindId {
	if len(val) <= 0 {
		return -1
	}

	return BindId(crc32.ChecksumIEEE([]byte(val)))
}
