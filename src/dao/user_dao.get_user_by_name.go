package dao

import (
	"xgame.go_server/comm/log"
	"xgame.go_server/model"
)

const sqlGetUserByName = `select user_id, user_name, password, hero_avatar, curr_hp from t_user where user_name = ?`

// GetUserByName 根据用户名称获取用户模型
func (this *UserDao) GetUserByName(userName string) *model.User {
	if len(userName) <= 0 {
		return nil
	}

	row := mySqlDb.QueryRow(sqlGetUserByName, userName)

	if nil == row {
		return nil
	}

	user := &model.User{}

	err := row.Scan(
		&user.UserId,
		&user.UserName,
		&user.Password,
		&user.HeroAvatar,
		&user.CurrHp,
	)

	if nil != err {
		log.Error("%+v", err)
		return nil
	}

	return user
}
