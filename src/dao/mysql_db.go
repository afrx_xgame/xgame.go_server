package dao

import (
	"database/sql"
	"time"

	_ "github.com/go-sql-driver/mysql"
)

var mySqlDb *sql.DB

func init() {
	var mysqlErr error
	mySqlDb, mysqlErr = sql.Open("mysql", "root:root@tcp(127.0.0.1:3306)/hero_story")

	if nil != mysqlErr {
		panic(mysqlErr)
	}

	// 最大连接数
	mySqlDb.SetMaxOpenConns(128)
	// 闲置连接数
	mySqlDb.SetMaxIdleConns(16)
	// 最大连接周期
	mySqlDb.SetConnMaxLifetime(2 * time.Minute)

	if mysqlErr = mySqlDb.Ping(); nil != mysqlErr {
		panic(mysqlErr)
	}
}
