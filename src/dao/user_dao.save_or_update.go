package dao

import (
	"xgame.go_server/comm/log"
	"xgame.go_server/model"
)

const sqlSaveOrUpdate = `
insert into t_user
( 
	user_name, 
    password, 
    hero_avatar, 
 	curr_hp,
 	create_time, 
 	last_login_time 
) 
value 
( 
    ?, ?, ?, ?, ?, ?
)
on duplicate key update curr_hp = values(curr_hp), last_login_time = values(last_login_time)
`

// SaveOrUpdate 保存或更新用户
func (this *UserDao) SaveOrUpdate(user *model.User) {
	if nil == user {
		return
	}

	stmt, err := mySqlDb.Prepare(sqlSaveOrUpdate)

	if nil != err {
		log.Error("%+v", err)
		return
	}

	result, err := stmt.Exec(
		user.UserName,
		user.Password,
		user.HeroAvatar,
		user.CurrHp,
		user.CreateTime,
		user.LastLoginTime,
	)

	if nil != err {
		log.Error("%+v", err)
		return
	}

	rowId, err := result.LastInsertId()

	if nil != err {
		log.Error("%+v", err)
		return
	}

	user.UserId = rowId
}
