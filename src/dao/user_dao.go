package dao

// 用户 DAO
type UserDao struct {
}

// 单例对象
var _userDao_singleton = &UserDao{}

// 获取用户 DAO
func GetUserDao() *UserDao {
	return _userDao_singleton
}
