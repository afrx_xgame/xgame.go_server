package service

type LoginService struct {
}

// 单例对象
var _loginService_singleton = &LoginService{}

// 获取登录服务
func GetLoginService() *LoginService {
	return _loginService_singleton
}
