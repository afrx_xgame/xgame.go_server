package service

import "xgame.go_server/model"

// LoginResult 登录结果
type LoginResult struct {
	*model.User
}
