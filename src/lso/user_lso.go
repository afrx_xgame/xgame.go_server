package lso

import (
	"fmt"

	"xgame.go_server/comm/async_op"
	"xgame.go_server/dao"
	"xgame.go_server/model"
)

// UserLso 用户延迟保存对象
type UserLso struct {
	*model.User
}

func (this *UserLso) GetLsoId() string {
	return fmt.Sprintf("User_%+v", this.UserId)
}

func (this *UserLso) SaveOrUpdate() {
	async_op.Process(
		async_op.BindId(this.UserId),
		func() {
			dao.GetUserDao().SaveOrUpdate(this.User)
		},
		nil,
	)
}

// GetUserLso 获取用户延迟保存对象
func GetUserLso(u *model.User) *UserLso {
	if nil == u {
		return nil
	}

	existComp, _ := u.GetComponentMap().Load("UserLso")

	if nil != existComp {
		return existComp.(*UserLso)
	}

	existComp = &UserLso{
		User: u,
	}

	existComp, _ = u.GetComponentMap().LoadOrStore("UserLso", existComp)

	return existComp.(*UserLso)
}
