package model

//
// MoveState 移动状态
type MoveState struct {
	FromPosX  float32
	FromPosY  float32
	ToPosX    float32
	ToPosY    float32
	StartTime int64
}
