package model

import "sync"

//
// User 用户模型
type User struct {
	UserId        int64  `db:"id"`
	UserName      string `db:"user_name"`
	Password      string `db:"password"`
	HeroAvatar    string `db:"hero_avatar"`
	CurrHp        int32  `db:"curr_hp"`
	CreateTime    int64  `db:"create_time"`
	LastLoginTime int64  `db:"last_login_time"`
	LastLoginIp   string `db:"last_login_ip"`
	MoveState     *MoveState

	// 组件字典
	componentMap *sync.Map

	// 临时锁
	tempLocker sync.Mutex
}

//
// GetComponentMap 获取组件字典
func (u *User) GetComponentMap() *sync.Map {
	if nil != u.componentMap {
		return u.componentMap
	}

	u.tempLocker.Lock()
	defer u.tempLocker.Unlock()

	// 加锁之后二次验证
	if nil != u.componentMap {
		return u.componentMap
	}

	u.componentMap = &sync.Map{}

	return u.componentMap
}
