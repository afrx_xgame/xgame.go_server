package cmd_handler

import "google.golang.org/protobuf/reflect/protoreflect"

//
// MyCmdCtx 自定义指令上下文,
// 在这里获取或绑定用户 Id, 发送消息等...
//
type MyCmdCtx interface {
	//
	// GetSessionUId 获取会话 Id
	GetSessionUId() string

	//
	// BindUserId 绑定用户 Id.
	// 在鉴权之后调用, 而且仅调用一次
	BindUserId(val int64)

	//
	// GetUserId 获取用户 Id.
	// 如果没有绑定用户 Id, 那么返回 -1
	GetUserId() int64

	//
	// GetClientIpAddr 获取客户端 IP 地址
	GetClientIpAddr() string

	//
	// SendError 发送错误.
	// errCode = 错误代码, errInfo 错误信息
	SendError(errCode int, errInfo string)

	//
	// Write 写出消息给客户端
	Write(msg protoreflect.ProtoMessage)

	//
	// Disconnect 断开客户端连接
	Disconnect()
}
