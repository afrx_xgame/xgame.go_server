package cmd_handler

import (
	"google.golang.org/protobuf/types/dynamicpb"
	"xgame.go_server/comm/log"
	"xgame.go_server/model"
	"xgame.go_server/msg"
)

// 初始化
func init() {
	handleFuncMap[int32(msg.MsgCode_USER_ENTRY_CMD.Number())] = handleUserEntryCmd
}

// 用户登录指令处理器
func handleUserEntryCmd(ctx MyCmdCtx, _ *dynamicpb.Message) {
	if nil == ctx ||
		ctx.GetUserId() <= 0 {
		return
	}

	log.Info(
		"收到用户入场消息! userId = %d",
		ctx.GetUserId(),
	)

	getBroadcaster().addCmdCtx(ctx)

	// 获取用户数据
	user := model.GetUserGroup().GetByUserId(ctx.GetUserId())

	if nil == user {
		log.Error(
			"未找到用户数据, userId = %d",
			ctx.GetUserId(),
		)
		return
	}

	userEntryResult := &msg.UserEntryResult{
		UserId:     uint32(ctx.GetUserId()),
		UserName:   user.UserName,
		HeroAvatar: user.HeroAvatar,
	}

	getBroadcaster().broadcast(userEntryResult)
}
