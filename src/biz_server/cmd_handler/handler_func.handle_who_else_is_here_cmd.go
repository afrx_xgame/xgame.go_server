package cmd_handler

import (
	"google.golang.org/protobuf/types/dynamicpb"
	"xgame.go_server/comm/log"
	"xgame.go_server/model"
	"xgame.go_server/msg"
)

func init() {
	handleFuncMap[int32(msg.MsgCode_WHO_ELSE_IS_HERE_CMD.Number())] = handleWhoElseIsHereCmd
}

// 处理还有谁指令
func handleWhoElseIsHereCmd(ctx MyCmdCtx, _ *dynamicpb.Message) {
	if nil == ctx ||
		ctx.GetUserId() <= 0 {
		return
	}

	log.Info(
		"收到“还有谁”消息! userId = %d",
		ctx.GetUserId(),
	)

	whoElseIsHereResult := &msg.WhoElseIsHereResult{}

	// 获得所有用户
	userALL := model.GetUserGroup().GetUserALL()

	for _, user := range userALL {
		if nil == user {
			continue
		}

		userInfo := &msg.WhoElseIsHereResult_UserInfo{
			UserId:     uint32(user.UserId),
			UserName:   user.UserName,
			HeroAvatar: user.HeroAvatar,
		}

		if nil != user.MoveState {
			// 将数据中的移动状体 同步到 消息上的移动状态
			userInfo.MoveState = &msg.WhoElseIsHereResult_UserInfo_MoveState{
				FromPosX:  user.MoveState.FromPosX,
				FromPosY:  user.MoveState.FromPosY,
				ToPosX:    user.MoveState.ToPosX,
				ToPosY:    user.MoveState.ToPosY,
				StartTime: uint64(user.MoveState.StartTime),
			}
		}

		whoElseIsHereResult.UserInfo = append(
			whoElseIsHereResult.UserInfo,
			userInfo,
		)
	}

	ctx.Write(whoElseIsHereResult)
}
