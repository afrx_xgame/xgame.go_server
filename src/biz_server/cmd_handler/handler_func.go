package cmd_handler

import "google.golang.org/protobuf/types/dynamicpb"

// HandlerFunc 指令处理器函数
type HandlerFunc func(ctx MyCmdCtx, pMsgObj *dynamicpb.Message)

// 指令处理器函数字典
var handleFuncMap = make(map[int32]HandlerFunc)

// GetHandlerFunc 获取指令处理器
func GetHandlerFunc(msgCode int32) HandlerFunc {
	return handleFuncMap[msgCode]
}
