package ws

import (
	"google.golang.org/protobuf/reflect/protoreflect"
	"xgame.go_server/comm/log"
	"xgame.go_server/msg"
)

// 办事员
type cmdCtx struct {
	gatewayServerId int32
	sessionUId      string
	userId          int64
	clientIpAddr    string
	*GatewayServerConn
}

// @Override
func (this *cmdCtx) GetSessionUId() string {
	return this.sessionUId
}

// @Override
func (this *cmdCtx) BindUserId(val int64) {
	this.userId = val
}

// @Override
func (this *cmdCtx) GetUserId() int64 {
	return this.userId
}

// @Override
func (this *cmdCtx) GetClientIpAddr() string {
	return this.clientIpAddr
}

// @Override
func (this *cmdCtx) Write(msgObj protoreflect.ProtoMessage) {
	if nil == msgObj ||
		nil == this.GatewayServerConn {
		return
	}

	byteArray, err := msg.Encode(msgObj)

	if nil != err {
		log.Error("%+v", err)
		return
	}

	innerCmd := &msg.InnerCmd{
		GatewayServerId: this.gatewayServerId,
		SessionUId:      this.sessionUId,
		UserId:          this.userId,
		MsgData:         byteArray,
	}

	this.GatewayServerConn.sendMsgQ <- innerCmd
}

// @Override
func (ctx *cmdCtx) SendError(errorCode int, errorInfo string) {
}

// @Override
func (ctx *cmdCtx) Disconnect() {
}
