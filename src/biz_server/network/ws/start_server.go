package ws

import (
	"net/http"
	"strconv"

	"github.com/gorilla/websocket"
	"xgame.go_server/cluster"
	"xgame.go_server/comm/log"
)

// 会话 Id
var nSessionUId int32 = 0

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
}

// StartServer 启动 Websocket 服务器
//
// bindHost = 绑定主机地址,
// bindPort = 绑定端口号,
// rootPath = 根目录, 例如: /ws 或者 /websocket
func StartServer(bindHost string, bindPort int, rootPath string) {
	if len(bindHost) <= 0 ||
		bindPort < 1000 {
		log.Error("主机地址或端口号不正确")
		return
	}

	go func() {
		http.HandleFunc(rootPath, websocketHandshake)
		_ = http.ListenAndServe(
			bindHost+":"+strconv.Itoa(bindPort),
			nil,
		)
	}()
}

// Websocket 握手,
// 其实就是处理 Websocket Upgrade 协议
func websocketHandshake(w http.ResponseWriter, r *http.Request) {
	// 升级为 WebSocket
	wsConn, err := upgrader.Upgrade(w, r, nil)

	if nil != err {
		log.Error("UpgradeError: %+v", err)
		return
	}

	if !cluster.IsFromGatewayServer(r) {
		log.Error("非网关服务器")
		return
	}

	defer func() {
		_ = wsConn.Close()
	}()

	nSessionUId++

	log.Info("有网关服务器连入")

	gatewayServerConn := &GatewayServerConn{
		wsConn: wsConn,
	}

	gatewayServerConn.startLoopSendMsg()
	gatewayServerConn.loopReadMsg()
}
