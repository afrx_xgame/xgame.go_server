package cluster

import "github.com/gorilla/websocket"

// 业务服务器数据
type BizServerData struct {
	ServerId  uint32          `json:"serverId"`
	BindHost  string          `json:"bindHost"`
	BindPort  uint32          `json:"bindPort"`
	SjtArray  []ServerJobType `json:"sjtArray"`
	LoadCount uint32          `json:"loadCount"`
	WsConn    *websocket.Conn `json:"-"` // 该字段不做序列化
}
