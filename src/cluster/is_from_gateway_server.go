package cluster

import "net/http"

// 是否来自网关服务器
func IsFromGatewayServer(r *http.Request) bool {
	if nil == r {
		return false
	}

	headerVal := r.Header.Get(gatewayServerHeaderKey)
	return gatewayServerHeaderVal == headerVal
}
