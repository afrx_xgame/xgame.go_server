package cluster

import (
	"strconv"
	"strings"
)

// ServerJobType 服务器职责类型, 简写为 sjt
type ServerJobType int16

const (
	E_SJT_LOGIN ServerJobType = iota + 1 // 1
	E_SJT_GAME                           // 2
)

// 转为字符串
func (sjt ServerJobType) ToStr() string {
	switch sjt {
	case E_SJT_LOGIN:
		return "LOGIN"
	case E_SJT_GAME:
		return "GAME"
	default:
		return ""
	}
}

// 字符串转为服务器职责类型
func StrToSjt(val string) ServerJobType {
	if strings.EqualFold(val, "LOGIN") || // 和 "LOGIN" 比较
		strings.EqualFold(val, strconv.Itoa(int(E_SJT_LOGIN))) { // 和 "1" 比较
		return E_SJT_LOGIN
	}

	if strings.EqualFold(val, "GAME") || // 和 "GAME" 比较
		strings.EqualFold(val, strconv.Itoa(int(E_SJT_GAME))) { // 和 "2" 比较
		return E_SJT_GAME
	}

	panic("无法转型为服务器职责类型")
}

// 字符串转为服务器职责类型数组
func StrToSjtArray(val string) []ServerJobType {
	if len(val) <= 0 {
		return nil
	}

	strArray := strings.Split(val, ",")
	var enumArray []ServerJobType

	for _, currStr := range strArray {
		sjt := StrToSjt(currStr)
		enumArray = append(enumArray, sjt)
	}

	return enumArray
}
