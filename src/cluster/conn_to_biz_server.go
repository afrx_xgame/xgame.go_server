package cluster

import (
	"fmt"
	"net/http"

	"github.com/gorilla/websocket"
	"xgame.go_server/comm/log"
)

// 连接到业务服务器
func connToBizServer(data *BizServerData) bool {
	if nil == data ||
		len(data.BindHost) <= 0 ||
		nil == data.SjtArray ||
		len(data.SjtArray) <= 0 {
		return false
	}

	serverId := data.ServerId
	_, ok := theBizServerDataGroup().getByServerId(serverId)

	if ok {
		// 如果已经连接,
		// 那就不要重复连接了...
		return false
	}

	// 获取服务器地址
	serverAddr := fmt.Sprintf(
		"ws://%s:%d/ws",
		data.BindHost, data.BindPort,
	)

	header := http.Header{}
	header.Add(gatewayServerHeaderKey, gatewayServerHeaderVal)

	// 创建到游戏服务器的连接
	newConn, _, err := websocket.DefaultDialer.Dial(serverAddr, header)

	if nil != err {
		log.Error("%+v", err)
		return false
	}

	log.Info("发现新的业务服务器并建立连接, %s", serverAddr)

	data.WsConn = newConn
	theBizServerDataGroup().add(data)
	return true
}
