package cluster

import "sync"

// 业务服务器数据组
type bizServerDataGroup struct {
	// 默认字典,
	// key = serverId,
	// val = *BizServerData
	defaultMap *sync.Map

	// 服务器职责类型字典,
	// key = sjt: ServerJobType
	// val = Map<serverId: uint32, _: uint32>
	sjtMap *sync.Map
}

// 单例对象
var _bizServerDataGroup_singleton = &bizServerDataGroup{
	defaultMap: &sync.Map{},
	sjtMap:     &sync.Map{},
}

// 获取单例对象
func theBizServerDataGroup() *bizServerDataGroup {
	return _bizServerDataGroup_singleton
}

// 添加业务服务器数据
func (this *bizServerDataGroup) add(data *BizServerData) {
	if nil == data ||
		nil == data.SjtArray ||
		len(data.SjtArray) <= 0 {
		return
	}

	// 保存数据
	this.defaultMap.Store(data.ServerId, data)

	for _, sjt := range data.SjtArray {
		serverIdSet, ok := this.sjtMap.Load(sjt)

		if !ok {
			serverIdSet = &sync.Map{}
			this.sjtMap.LoadOrStore(sjt, serverIdSet)
		}

		serverIdSet, ok = this.sjtMap.Load(sjt)

		if !ok {
			panic("内置字典为空")
		}

		serverIdSet.(*sync.Map).Store(
			data.ServerId,
			0, // 无用值, 主要是为了模拟 Set
		)
	}
}

// 删除业务服务器数据
func (this *bizServerDataGroup) delete(data *BizServerData) {
	if nil == data ||
		nil == data.SjtArray ||
		len(data.SjtArray) <= 0 {
		return
	}

	this.defaultMap.Delete(data.ServerId)

	for _, sjt := range data.SjtArray {
		serverIdSet, ok := this.sjtMap.Load(sjt)

		if !ok {
			continue
		}

		serverIdSet.(*sync.Map).Delete(data.ServerId)
	}
}

// 删除业务服务器数据
func Delete(data *BizServerData) {
	theBizServerDataGroup().delete(data)
}

// 根据服务器 Id 获取业务服务器数据
func (this *bizServerDataGroup) getByServerId(serverId uint32) (*BizServerData, bool) {
	bizServerData, ok := this.defaultMap.Load(serverId)

	if !ok {
		return nil, false
	}

	return bizServerData.(*BizServerData), true
}
