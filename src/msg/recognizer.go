package msg

import (
	"errors"
	"google.golang.org/protobuf/reflect/protoreflect"
	"strings"
	"sync"
	"sync/atomic"
)

// msgCode --> msgDesc 字典
var msgCodeAndMsgDescMap = make(map[int32]protoreflect.MessageDescriptor)

// msgName --> msgCode 字典
var msgNameAndMsgCodeMap = make(map[string]int32)

// 是否已经初始化
var initOk int32 = 0

// 初始化锁
var initMapLocker = &sync.Mutex{}

// 根据消息代号获取消息描述
func getMsgDescByMsgCode(msgCode int32) (protoreflect.MessageDescriptor, error) {
	if msgCode < 0 {
		return nil, errors.New("消息代码无效")
	}

	// 初始化两个字典
	tryInit2Map()

	return msgCodeAndMsgDescMap[msgCode], nil
}

// 根据消息名称获取消息代号
func getMsgCodeByMsgName(msgName string) (int32, error) {
	if len(msgName) <= 0 {
		return -1, errors.New("消息名称为空")
	}

	// 初始化两个字典
	tryInit2Map()

	return msgNameAndMsgCodeMap[msgName], nil
}

// 尝试初始化两个字典
func tryInit2Map() {
	if 1 == atomic.LoadInt32(&initOk) {
		return
	}

	initMapLocker.Lock()
	defer initMapLocker.Unlock()

	if 1 == atomic.LoadInt32(&initOk) {
		return
	}

	var tempMap = make(map[string]int32)

	for k, v := range MsgCode_value {
		// 清除下划线 & 全部转为小写
		// 例如: USER_LOGIN_CMD ==> userlogincmd
		msgId := strings.ToLower(
			strings.Replace(k, "_", "", -1),
		)

		tempMap[msgId] = v
	}

	msgDescList := File_GameMsgProtocol_proto.Messages()

	for i := 0; i < msgDescList.Len(); i++ {
		msgDesc := msgDescList.Get(i)
		// 获得消息名称之后, 也把消息名称清除下划线 & 全部转为小写
		// 例如: UserLoginCmd ==> userlogincmd
		msgName := string(msgDesc.Name())
		msgId := strings.ToLower(
			strings.Replace(msgName, "_", "", -1),
		)

		msgCode := tempMap[msgId]

		msgNameAndMsgCodeMap[msgName] = msgCode
		msgCodeAndMsgDescMap[msgCode] = msgDesc
	}

	atomic.CompareAndSwapInt32(&initOk, 0, 1)
}
