package ws

import (
	"sync"

	"xgame.go_server/comm/log"
)

// 指令上下文组
type cmdCtxGroup struct {
	defaultMap *sync.Map // Map<String, CmdContextImpl>
}

// 单例对象
var _cmdCtxGroup_singleton = &cmdCtxGroup{
	defaultMap: &sync.Map{},
}

// TheCmdCtxGroup 获取指令上下文分组
func TheCmdCtxGroup() *cmdCtxGroup {
	return _cmdCtxGroup_singleton
}

// Add 添加指令上下文对象
func (this *cmdCtxGroup) Add(newCtx *cmdCtx) {
	if nil == newCtx {
		return
	}

	log.Info("添加指令上下文, sessionUId = %s", newCtx.sessionUId)
	this.defaultMap.Store(newCtx.sessionUId, newCtx)
}

// DeleteBySessionUId 删除指令上下文对象
func (this *cmdCtxGroup) DeleteBySessionUId(sessionUId string) {
	if len(sessionUId) <= 0 {
		return
	}

	this.defaultMap.Delete(sessionUId)
}

// GetBySessionUId 根据 sessionUId 获取指令上下文对象
func (this *cmdCtxGroup) GetBySessionUId(sessionUId string) *cmdCtx {
	if len(sessionUId) <= 0 {
		return nil
	}

	obj, ok := this.defaultMap.Load(sessionUId)

	if !ok {
		return nil
	}

	return obj.(*cmdCtx)
}
