@echo off

rem del ..\msg\*.pb.go

protoc --go_out=..\ .\GameMsgProtocol.proto

ren ..\msg\GameMsgProtocol.pb.go game_msg_protocol.pb.go

pause
